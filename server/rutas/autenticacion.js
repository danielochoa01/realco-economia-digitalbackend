const Inmueble = require('../modelos/inmueble')
let verificarAutenticacion = (req,res,next) => {
    if(req.session._id) {
        return next();
    } else {
        res.status(401).json({
            mensaje:'Acceso no autorizado'
        })
    }
}

let verificarUsuario=(req,res,next)=>{
    let id=req.params.id;
    let idUsuario=req.session._id;
    Inmueble.findOne({$and:[{_id:id},{arrendador:idUsuario}]},(error,inmueble)=>{
        if(inmueble){
            return next();
        }else{
            res.status(403).json({
                mensaje:'No tiene los permisos para realizar esta accion'
            })
        }
    })
}
module.exports = {
    verificarAutenticacion,
    verificarUsuario
}