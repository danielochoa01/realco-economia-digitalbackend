const express = require('express');
const _ = require('underscore');
const app = express();
let { verificarAutenticacion,verificarUsuario } = require('./autenticacion');
const Usuario = require('../modelos/usuario');
const Inmueble = require("../modelos/inmueble");
const fs = require('fs');
const path = require('path');
const googleStorage = require('@google-cloud/storage')
require('../config/config')


const storage = new googleStorage.Storage({
    projectId: "realco-aa328",
    keyFilename: path.join(__dirname,'..','/config/realco-aa328-firebase-adminsdk-5n8bm-47a123e930.json')
  });
  
  const bucket = storage.bucket("realco-aa328.appspot.com");
// const Multer = require('multer');

//const multer = require('multer')
/*var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        console.log(file.fieldname);
      cb(null, '../../uploads/')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now())
    }
  })*/
   
//const upload =multer({ dest: './uploads/' })

const apiResponse = (req,res,error,inmuebles,filtrar) => {
    if (error) {
        return res.status(500).json({
            error
        });
    }
    if (inmuebles.length === 0) {
        return res.status(404).json({
            mensaje:'Pagina no Encontrada'
        });
    }
    Inmueble.countDocuments({$and:filtrar},(err,conteo) => {
        res.json({
            inmuebles,
            paginas:Math.ceil(conteo/items)
        })
    })
}
const filtros = (req) => {
    let municipio=req.query.municipio;
    let departamento=req.query.departamento
    var filtros = [{estado:true}]
    let minP = Number(req.query.minP) || 0
    let maxP = Number(req.query.maxP) || 0
    let tipo = req.query.tipo || '';
    let minT= Number(req.query.minT)|| 0;
    let maxT = (req.query.maxT) || 0;
    if (maxP === 0) {
        filtros.push({precio:{$gte:minP}});
    } else {
        filtros.push({precio:{$gte:minP,$lte:maxP}});
    }
    if (maxT === 0) {
        filtros.push({tamano:{$gte:minT}})
    } else {
        filtros.push({tamano:{$gte:minT,$lte:maxT}})
    }
    if (tipo !== '') {
        filtros.push({tipo:tipo})
    }
    if(departamento){
        filtros.push({'ubicacion.departamento':departamento})
    }
    if(municipio){
        filtros.push({'ubicacion.municipio':municipio})
    }
    return filtros;
}

const apiResponseUsuario = (req,res,error,usuario) => {
    if (error){
        return res.status(500).json({
            error
        })
    }
    if(!usuario){
        return res.status(400).json({
            mensaje:'Usuario no encontrado'
        })
    }
    res.status(200).json({
        usuario
    })
}

app.post('/inmueble',verificarAutenticacion,async (req,res) => {
    let inmueble = JSON.parse(req.body.inmueble);
    let id = req.session._id;;
    if(req.files){
        let archivo = req.files.archivo;
        let nombreCortado = archivo.name.split('.');
        let extension = nombreCortado[nombreCortado.length - 1];
        // Extensiones permitidas
        let extensionesValidas = ['png', 'jpg', 'gif', 'jpeg'];

        if (extensionesValidas.indexOf(extension) < 0) {
            return res.status(418).json({
                    mensaje: 'Las extensiones permitidas son ' + extensionesValidas.join(',')
            })
        }
        try {
            let nombreArchivo = await uploadImageToStorage(archivo);
            // Aqui, imagen cargada
            console.log(req.body);
            let foto = [{url:nombreArchivo,nombre:'nombre',descripcion:'descripcion'}]
            //let inmueble=req.body
            inmueble.fotos = foto;
        } catch (error) {
            return res.status(501).json({
                ok: false,
                error
            });
        }
    }
    inmueble.arrendador = id
    nuevoInmueble = new Inmueble(inmueble);  
    console.log(nuevoInmueble);  
    nuevoInmueble.save((error,inmuebleGuardado)=>{
        if(error){
            return res.status(500).json({
                error
            })
        }
        return res.status(200).json({
            inmueble:inmuebleGuardado,
            mensaje: 'El inmueble se guardo Exitosamente'
        })
    });
    /*nuevoInmueble=new Inmueble({
        arrendador:id,
        precio:inmueble.precio,
        tipo:inmueble.tipo,
        tamano:inmueble.tamano,
    });*/
})

app.put('/agregar-imagen/:id',(req,res)=>{
    let id=req.params.id
    if (!req.files) {
        return res.status(404)
            .json({
                mensaje:'No se ha seleccionado ningún archivo'
            });
    }
    let archivo = req.files.archivo;
    let nombreCortado = archivo.name.split('.');
    let extension = nombreCortado[nombreCortado.length - 1];

    // Extensiones permitidas
    let extensionesValidas = ['png', 'jpg', 'gif', 'jpeg'];

    if (extensionesValidas.indexOf(extension) < 0) {
        return res.status(418).json({
                mensaje: 'Las extensiones permitidas son ' + extensionesValidas.join(',')
        })
    }
    uploadImageToStorage(archivo).then((nombreArchivo) => {
        foto = {url:nombreArchivo,nombre:'nombre',descripcion:'descripcion'}
        Inmueble.findByIdAndUpdate(id,{$push:{fotos:foto}},{new:true},(error,inmuebleActualizado)=>{
            if (error){
                return res.status(500).json({
                    error
                })
            }
            if(!inmuebleActualizado){
                return res.status(404).json({
                    mensaje:'inmueble no Encontrado'
                })
            }
            res.json({
                fotosInmueble:inmuebleActualizado.fotos,
                mensaje:'La imagen ha sido agregada con exito'
            })
        })

        // res.status(200).send({
        //   status: 'success'
        // });
      }).catch((error) => {
        res.status(500).json({
            ok: false,
            error
        });
      });
    // let nombreArchivo = `${ id }-${ new Date().getMilliseconds()  }.${ extension }`;
    // archivo.mv(`../uploads/${ nombreArchivo }`, (err) => {

    //     if (err){
    //         return res.status(500).json({
    //             ok: false,
    //             err
    //         });
    //     }
    //     // Aqui, imagen cargada
    //     foto = {url:nombreArchivo,nombre:'nombre',descripcion:'descripcion'}
    //     Inmueble.findByIdAndUpdate(id,{$push:{fotos:foto}},{new:true},(error,inmuebleActualizado)=>{
    //         if (error){
    //             return res.status(500).json({
    //                 error
    //             })
    //         }
    //         if(!inmuebleActualizado){
    //             return res.status(404).json({
    //                 mensaje:'inmueble no Encontrado'
    //             })
    //         }
    //         res.json({
    //             fotosInmueble:inmuebleActualizado.fotos,
    //             mensaje:'La imagen ha sido agregada con exito'
    //         })
    //     })

    // });
});

app.get('/inmuebles-home',(req,res) => {
    let pagina = req.query.pagina || 1
    pagina = Number(pagina);
    let desde = (pagina-1)*items
    let filtrar = filtros(req);
    Inmueble.find({$and:filtrar},{precio:1,tipo:1,ubicacion:1,fotos:1,fechaPublicacion:1})
    .skip(desde)
    .limit(items)
    .sort({fechaPublicacion:-1})
    .exec((error,inmuebles) => apiResponse(req,res,error,inmuebles,filtrar))
});

app.get('/inmueble/:id', async (req,res) => {
    let id = req.params.id;
    let idUsuario = req.session._id
    const usuario = await Usuario.findById(idUsuario,{favoritos:1})
    if(usuario){
        var posicion = usuario.favoritos.indexOf(id)
    }
    if (usuario && posicion != -1) {
    Inmueble.findOne({$and:[{_id:id},{estado:true}]})
    .populate('arrendador','nombre telefono email')
    .exec((error,inmueble) => {
        if (error) {
            return res.status(500).json({
                error
            });
        }
        if (!inmueble) {
            return res.status(404).json({
                 mensaje: 'Inmueble user no encontrado'
            });
        }
        res.json({
            inmueble,
            favorito:true
        });
    }) 
    } else {
        Inmueble.findOne({$and:[{_id:id},{estado:true}]})
        .populate('arrendador','nombre telefono email')
        .exec((error,inmueble) => {
            if (error) {
                return res.status(500).json({
                    error
                });
            }
            if (!inmueble) {
                return res.status(404).json({
                     mensaje: 'Inmueble no encontrado'
                });
            }
            res.json({
                inmueble,
                favorito:false
            });
        }) 
    }
})

app.get('/inmuebles/:busqueda',(req,res) => {
    let busqueda = req.params.busqueda;
    let pagina = req.query.pagina || 1
    pagina = Number(pagina);
    let desde = (pagina-1)*items
    let filtrar = filtros(req);
    filtrar.push({$text:{$search:busqueda}})
    console.log(filtrar);
    //let filtrar={$and:[ {$text:{$search:busqueda}},{estado:true}]}                  
    Inmueble.find({$and:filtrar},{precio:1,tipo:1,ubicacion:1,fotos:1,fechaPublicacion:1})
    .skip(desde)
    .limit(items)
    .sort({fechaPublicacion:-1})
    .exec((error,inmuebles)=>apiResponse(req,res,error,inmuebles,filtrar))
});

app.get('/inmuebles',(req,res) => {
    let pagina = req.query.pagina || 1
    pagina = Number(pagina);
    let desde = (pagina-1)*items
    let filtrar = filtros(req);
    console.log(filtrar);
    //let filtrar={$and:[ {$text:{$search:busqueda}},{estado:true}]}                  
    Inmueble.find({$and:filtrar},{precio:1,tipo:1,ubicacion:1,fotos:1,fechaPublicacion:1})
    .skip(desde)
    .limit(items)
    .sort({fechaPublicacion:-1})
    .exec((error,inmuebles)=>apiResponse(req,res,error,inmuebles,filtrar))
});

app.get('/inmueblesFiltro',(req,res) => {
    let pagina = Number(req.query.pagina) || 1
    let desde = (pagina-1)*items
    let filtrar = filtros(req);
    Inmueble.find({$and:filtrar},{precio:1,tipo:1,ubicacion:1,fotos:1,fechaPublicacion:1})
            .skip(desde)
            .limit(items)
            .sort({fechaPublicacion:-1})
            .exec((error,inmuebles) => apiResponse(req,res,error,inmuebles,filtros))
    
});

app.put('/inmueble/:id',verificarAutenticacion,verificarUsuario,(req,res) => {
    let id = req.params.id;
    let inmueble = req.body
    //Inmueble.findByIdAndUpdate(id,inmueble,{new:true,runValidators:true},(error,inmuebleActualizado))
    Inmueble.findByIdAndUpdate(id,inmueble,{new:true,runValidators:true},(error,inmuebleActualizado)=>{
        if (error) {
            return res.status(500).json({
                error
            })
        }
        if (!inmuebleActualizado) {
            return res.status(404).json({
                mensaje:'Usuario no Encontrado'
            })
        }
        res.json({
            inmueble:inmuebleActualizado,
            mensaje:'El usuario ha sido actualizado'
        })
    })
})

app.delete('/inmueble/:id',verificarAutenticacion,verificarUsuario,(req,res) => {
    let id = req.params.id;
    Inmueble.findByIdAndUpdate(id,{estado:false},{new:true,runValidators:true},(error,inmuebleBorrado)=>{
        if (error) {
            return res.status(500).json({
                error
            })
        }
        if (!inmuebleBorrado) {
            return res.status(404).json({
                mensaje:'Inmueble no encontrado'
            })
        }
        res.json({
            mensaje:'Inmueble Borrado',
            inmueble:inmuebleBorrado
        })
    })

})

app.post('/favorito/:id',verificarAutenticacion,(req,res) => {
    let id = req.params.id;
    let idUsuario = req.session._id
    Usuario.findById(idUsuario,{favoritos:1},(error,usuario) => {
        if (error) {
            return res.status(500).json({
                error
            })
        }
        let posicion = usuario.favoritos.indexOf(id)
        if (posicion === -1) {
            Usuario.findByIdAndUpdate(idUsuario,{$push:{favoritos:id}},{new:true},(error,usuarioActualizado)=>apiResponseUsuario(req,res,error,usuarioActualizado))
        }else{
            Usuario.findByIdAndUpdate(idUsuario,{$pull:{favoritos:id}},{new:true},(error,usuarioActualizado)=>apiResponseUsuario(req,res,error,usuarioActualizado))
        }
    })
})

app.get('/favoritos',verificarAutenticacion,(req,res) => {
    let pagina = Number(req.query.pagina) || 1
    let desde = (pagina-1)*items
    let idUsuario = req.session._id
    Usuario.findById(idUsuario,{favoritos:1},(error,usuario) => {
        if (error) {
            return res.status(500).json({
                error
            })
        }
        Inmueble.find({$and:[{_id:{ $in:usuario.favoritos}},{estado:true}]},{precio:1,tipo:1,ubicacion:1,fotos:1,fechaPublicacion:1})
        .skip(desde)
        .limit(items)
        .sort({fechaPublicacion:-1})
        .exec((error,inmuebles) => {
            if (inmuebles.length === 0) {
                return res.status(404).json({
                    mensaje:'Pagina no Encontrada'
                });
            }
            Inmueble.countDocuments({$and:[{_id:{ $in:usuario.favoritos}},{estado:true}]},(err,conteo)=>{
                res.json({
                    inmuebles,
                    paginas:Math.ceil(conteo/items)
                })
            })
        })
    })
});

app.get('/inmueblesPrecio',(req,res) => {
    let min = Number(req.query.min) || 0
    let max = Number(req.query.max) || 0
    let pagina = req.query.pagina || 1
    pagina = Number(pagina);
    let desde = (pagina-1)*items
    if (max === 0) {
        Inmueble.find({$and:[{precio:{$gte:min}},{estado:true}]}) 
        .skip(desde)
        .limit(items)
        .sort({fechaPublicacion:-1})
        .exec((error,inmuebles)=>apiResponse(req,res,error,inmuebles))
    } else {
        Inmueble.find({$and:[{precio:{$gte:min,$lte:max}},{estado:true}]})
        .skip(desde)
        .limit(5)
        .sort({fechaPublicacion:-1})
        .exec((error,inmuebles) => apiResponse(req,res,error,inmuebles))
    }
});

app.get('/inmuebles-usuario',verificarAutenticacion,(req,res) => {
    let pagina = Number(req.query.pagina) || 1
    let desde = (pagina-1)*items
    let idUsuario = req.session._id
    let filtrar=[{arrendador:idUsuario},{estado:true}];
    Inmueble.find({$and:filtrar},{precio:1,tipo:1,ubicacion:1,fotos:1,fechaPublicacion:1})
    .skip(desde)
    .limit(items)
    .sort({fechaPublicacion:-1})
    .exec((error,inmuebles) => apiResponse(req,res,error,inmuebles,filtrar));
})

app.get('/inmuebles',(req,res) => {
    let pagina = req.query.pagina || 1
    pagina = Number(pagina);
    let desde = (pagina-1)*items
    let filtrar = {estado:true}
    Inmueble.find(filtrar,{precio:1,tipo:1,ubicacion:1,fotos:1,fechaPublicacion:1})
    .skip(desde)
    .limit(items)
    .sort({fechaPublicacion:-1})
    .exec((error,inmuebles) => apiResponse(req,res,error,inmuebles,filtrar))
})
/*app.get('/inmueblesPaginacion',(req,res)=>{
    let pagina=req.query.pagina || 1
    pagina=Number(pagina);
    let desde=(pagina-1)*5
    Inmueble.find({estado:true})
    .skip(desde)
    .limit(5)
    .exec((error,inmuebles)=>{
        if (error) {
            return res.status(500).json({
                error
            });
        }
        if(inmuebles.length===0){
            return res.status(404).json({
                mensaje:'Pagina no Encontrada'
            });
        }
        res.json({
            inmuebles
        })
    })
})
*/
/*app.get('/inmuebles/:busqueda',(req,res)=>{
    let busqueda=req.params.busqueda;
    Inmueble.find({$or:[
        {'ubicacion.colonia':busqueda},
        {'ubicacion.departamento':busqueda},
        {'ubicacion.municipio':busqueda}
    ]})
    .exec((error,inmuebles)=>{
        if (error) {
            return res.status(500).json({
                error
            });
        }
        if(inmuebles.length===0){
            return res.status(404).json({
                mensaje:' no Encontrado'
            });
        }
        res.json({
            inmuebles
        })
    })
//
});
*/
/**
 * Upload the image file to Google Storage
 * @param {File} file object that will be uploaded to Google Storage
 */
const uploadImageToStorage = (file) => {
    console.log('fn');
  let prom = new Promise((resolve, reject) => {
    if (!file) {
      reject('No image file');
    }
    let newFileName = `${file.name}_${Date.now()}`;

    let fileUpload = bucket.file(newFileName);

    const blobStream = fileUpload.createWriteStream({
      metadata: {
        contentType: file.mimetype
      }
    });

    blobStream.on('error', (error) => {
        console.log(error)
      reject('Something is wrong! Unable to upload at the moment.');
    });

    blobStream.on('finish', () => {
      // The public URL can be used to directly access the file via HTTP.
    //   const url = `https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`;
      const url = `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/${fileUpload.name}?alt=media`
      console.log(url)
      resolve(url);
    });

    blobStream.end(file.data);
  });
  return prom;
}

module.exports = app; 