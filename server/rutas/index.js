const express = require('express');
const app = express();
app.use(require('./inmueble'));
app.use(require('./usuario'));

module.exports = app;