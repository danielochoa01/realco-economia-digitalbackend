const express = require('express');
const _ = require('underscore');
const path = require('path');
const app = express();

app.get('/', function(req, res) {
    res.status(200).sendFile(path.join(__dirname,'../public','/index.html'));
});

module.exports = app;