const express = require('express');
const bcrypt = require('bcrypt');
const _ = require('underscore');
const app = express();
const Usuario = require('../modelos/usuario');
// const fileupload = require('express-fileupload');
const fs = require('fs');
const path = require('path');
let { verificarAutenticacion } = require('./autenticacion');
const googleStorage = require('@google-cloud/storage')
// app.use(fileupload());

const storage = new googleStorage.Storage({
    projectId: "realco-aa328",
    keyFilename: path.join(__dirname,'..','/config/realco-aa328-firebase-adminsdk-5n8bm-47a123e930.json')
});

const bucket = storage.bucket("realco-aa328.appspot.com");


app.post('/usuario', (req, res) => {
    let user = req.body
    console.log(user);
    
    Usuario.findOne({ email: user.email}, (error, UsuarioDB) => {
        if (error) {
            return res.status(500).json({
                error
            })
        }
        if (UsuarioDB) {
            return res.status(404).json({
                mensaje: 'El usuario ya existe'
            })
        } else {
            let usuario = new Usuario({
                nombre: user.nombre,
                email: user.email,
                contrasena: bcrypt.hashSync(user.contrasena, 10),
                numeroIdentidad: user.numeroIdentidad,
                fechaNacimiento: user.fechaNacimiento,
                genero: user.genero,
                telefono: user.telefono
            });
            usuario.save((err, usuarioGuardar) => {
                if (err) {
                    console.log(err);
                    
                    return res.status(500).json({
                        err
                    })
                }
                console.log(usuarioGuardar);
                return res.json({
                    usuario:usuarioGuardar,
                    mensaje: 'El usuario se guardo Exitosamente'
                })
            })
        }


    })
})

app.post('/login', (req, res) => {
    let body = req.body;
    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {

        if (err) {
            return res.status(500).json({
                err
            });
        }

        if (!usuarioDB) {
            return res.status(404).json({
                    mensaje: '(Usuario) o contraseña incorrectos'
            });
        }


        if (!bcrypt.compareSync(body.contrasena, usuarioDB.contrasena)) {
            return res.status(404).json({
                mensaje: 'Usuario o (contraseña) incorrectos'
            });
        }
        //_id
        req.session._id = usuarioDB._id;

        res.json({
            usuario: usuarioDB
        });
    });

});

app.put('/usuario',verificarAutenticacion,(req,res)=>{
    let id = req.session._id
    let user = _.pick( req.body,['nombre','estadoCivil','fechaNacimiento','telefono','RTN']);
    console.log(req.body);
    Usuario.findOneAndUpdate(id,user,{new:true},(error,usuarioDB) => {
        if (error){
            return res.status(500).json({
                error
            })
        }
        if(!usuarioDB){
            return res.status(404).json({
                mensaje:'Usuario no Encontrado'
            })
        }
        res.json({
            usuario: usuarioDB,
            mensaje:'El usuario ha sido actualizado'
        })
    })
})
app.delete('/usuario/:id',verificarAutenticacion,(req,res)=>{
    let id = req.params.id;
    Usuario.findByIdAndUpdate(id,{estado:false},{new:true,runValidators:true},(error,usuarioBorrado)=>{
        if (error){
            return res.status(500).json({
                error
            })
        }
        if(!usuarioBorrado){
            return res.status(400).json({
                mensaje:'Usuario no encontrado'
            })
        }
        res.json({
            mensaje:'Usuario borrado',
            usuario:usuarioBorrado
        })
    })
})

app.get('/obtenerUsuario',verificarAutenticacion,(req,res)=>{
    let id = req.session._id
    Usuario.findById(id,(error,usuario)=>{
        if(error){
            return res.status(500).json({
                error
            })
        }
        return res.json({
            usuario
        });
    })
})

app.get('/logout',verificarAutenticacion,(req,res)=>{
    req.session.destroy();
    res.status(200).json({
        mensaje:'hola que hace'
    })
});

app.get('/esta-logueado', (req, res) => {
    console.log('Estoy en esta-logueado');
    
    console.log(req.session._id);

    if (req.session._id) {
        res.json({
            estaLogueado: true,
            mensaje: `Logueado con id de usuario ${req.session._id}`
        });
    } else {
        res.json({
            estaLogueado: false,
            mensaje: `No está logueado`
        });
    }
    
});

app.put('/perfil',verificarAutenticacion,async (req,res) => {
    let id = req.session._id;
    if (!req.files) {
        return res.status(404)
            .json({
                mensaje:'No se ha seleccionado ningún archivo'
            });
    }
    let archivo = req.files.archivo;
    let nombreCortado = archivo.name.split('.');
    let extension = nombreCortado[nombreCortado.length - 1];

    // Extensiones permitidas
    let extensionesValidas = ['png', 'jpg', 'gif', 'jpeg'];

    if (extensionesValidas.indexOf(extension) < 0) {
        return res.status(418).json({
                mensaje: 'Las extensiones permitidas son ' + extensionesValidas.join(',')
        })
    }

    let nombreArchivo = `${ id }-${ new Date().getMilliseconds()  }.${ extension }`;
    try {
        let nombreArchivo = await uploadImageToStorage(archivo);
        // Aqui, imagen cargada
        Usuario.findByIdAndUpdate(id,{ imgPerfil:nombreArchivo },{new:true,runValidators:true},(error,usuarioDB)=>{
            if (error){
                return res.status(500).json({
                    error
                })
            }
            if(!usuarioDB){
                return res.status(404).json({
                    mensaje:'Usuario no Encontrado'
                })
            }
            res.json({
                usuario:usuarioDB,
                mensaje:'El usuario ha sido actualizado'
            })
        })
    } catch (error) {
        return res.status(500).json({
            ok: false,
            error
        });
    }
});

app.get('/imagen/:imagen',verificarAutenticacion,(req,res) => {

    let img = req.params.imagen;

    let pathImagen = path.resolve(__dirname, `../../../uploads/${ img }`);
    console.log(pathImagen);
    if (fs.existsSync(pathImagen)) {
        res.sendFile(pathImagen);
    } else {
        let noImagePath = path.resolve(__dirname, '../assets/no-image.jpg');
        res.sendFile(noImagePath);
    }

})
const uploadImageToStorage = (file) => {
    console.log('fn');
  let prom = new Promise((resolve, reject) => {
    if (!file) {
      reject('No image file');
    }
    let newFileName = `${file.name}_${Date.now()}`;

    let fileUpload = bucket.file(newFileName);

    const blobStream = fileUpload.createWriteStream({
      metadata: {
        contentType: file.mimetype
      }
    });

    blobStream.on('error', (error) => {
        console.log(error)
      reject('Something is wrong! Unable to upload at the moment.');
    });

    blobStream.on('finish', () => {
      // The public URL can be used to directly access the file via HTTP.
    //   const url = `https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`;
      const url = `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/${fileUpload.name}?alt=media`
      console.log(url)
      resolve(url);
    });

    blobStream.end(file.data);
  });
  return prom;
}
module.exports = app