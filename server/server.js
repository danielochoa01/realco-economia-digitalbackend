const express = require('express');
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cookieParser=require('cookie-parser');
const session = require('express-session');
const cors = require('cors');
const logger = require('morgan');
const fileupload = require('express-fileupload');
const history = require('connect-history-api-fallback');
const connect = require('connect');

const corsOptions = {
    origin: true,
    credentials: true,
    optionsSuccessStatus: 200,
    methods: ['GET', 'PUT', 'POST']
}


var app = express();
app.use(history({
  htmlAcceptHeaders: ['text/html', 'application/xhtml+xml'],
  verbose: true
}));
app.use(fileupload());
app.use(logger('dev'))
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(express.urlencoded({extended:true}));
app.use(cookieParser());
app.use(session({secret:'123456',resave:true,saveUninitialized:true}))
require("./config/config.js")

app.use(express.static(__dirname+"/public"));
//configuracion de rutas
app.use('/api', require('./rutas/index'));
app.use(require('./rutas/front'));
//base de datos
mongoose.connect(process.env.URLDB, {
    useCreateIndex: true,
    useNewUrlParser: true
  },(error) => {
    if (error) throw error;
    console.log('Base de Datos Online')
});
app.use(express.static(__dirname+"/public"));
//puerto
app.listen(process.env.PORT,function(){
    console.log(`Se ha iniciado el servidor en http://localhost:${process.env.PORT}`);
});
