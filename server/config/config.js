//Puerto
process.env.PORT=process.env.PORT || 3000;

//  Entorno
process.env.NODE_ENV = process.env.NODE_ENV || ':v';

//numero de inmuebles por pagina
items=5;

//base de datos
let urlDB;

if (process.env.NODE_ENV === 'dev') {
    urlDB = 'mongodb://localhost:27017/realco';
} else {
    urlDB = 'mongodb://desarrollador:asd.456@ds243049.mlab.com:43049/realco';
}
process.env.URLDB = urlDB;
