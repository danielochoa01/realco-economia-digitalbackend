const passport = require('passport')
const Usuario = require('../modelos/usuario');
const bcrypt = require('bcrypt');
const localStrategy = require('passport-local').Strategy()

passport.use('registroLocal',new localStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  
}))