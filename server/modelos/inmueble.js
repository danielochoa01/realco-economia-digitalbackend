const mongose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
let Schema = mongose.Schema;
let inmuebleSchema = new Schema ({
    arrendador:{
        type:Schema.Types.ObjectId, ref :'Usuario'
    },
    descripcion:{
        type:String,
        required:false
    },
    precio:{
        type:Number,
        required:[true,'El precio es obligatorio']
    },
    tamano:{
        type: Number,
        required:[true,'El tamano es obligatoriro']
    },
    tipo:{
        type:String,
        required:[true,'El tipo de inmueble es obligatorio']
    },
    contiene:{
        habitaciones:{
            type:Number,
            required:false,
        },
        banos:{
            type:Number,
            required:false,
        }
    },
    servicios:{
        tv:{
            type:Boolean,
            default:false
        },
        luz:{
            type:Boolean,
            default:false
        },
        agua:{
            type:Boolean,
            default:false
        }
    },
    fotos:[{
        url:String,
        nombre:String,
        descripcion:String
    }],
    requisitos:{
        genero:String,
        trabajo:String,
        estadoCivil:String,
        personasPermitidas:Number,
        mascota:{
            type:Boolean,
            default:false
        }
    },
    ubicacion:{
        departamento:String,
        municipio:String,
        colonia:String,
        NumeroCasa:Number,
        bloque:Number,
    },
    estado:{
        default:true,
        type: Boolean
    },
    fechaPublicacion:{
        type: Date,
        default:Date.now()
    }
})
inmuebleSchema.index()
module.exports = mongose.model('Inmueble',inmuebleSchema);