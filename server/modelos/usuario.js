const mongose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
let Schema = mongose.Schema;
let estadosValidos = {
    values:['Soltero','Casado','Union Libre','Viudo','Divorciado'],
    message:'{VALUE} no es un Estado Civil Valido'
}
let usuarioSchema = new Schema({
    nombre:{
        type:String,
        required:[true,'El nombre es obligatorio']
    },
    tipoUsuario:{
        type:Number,
        required:[true,'El tipo de usuario es obligatorio'],
        default:1,
    },
    email:{
        type:String,
        required:[true,'El Correo es obligatorio'],
        unique:true,
        lowercase:true,
    },
    contrasena:{
        type:String,
        required:[true,'La contrasena es Obligatorio']
    },
    numeroIdentidad:{
        type:String,
        unique:true,
        required:[true,'La identidad es obligatoria']
    },
    RTN:{
        type:String,
        unique:false,
        sparse: true
    },
    estadoCivil:{
        type:String,
        default:'Soltero',
        enum:estadosValidos
    },
    imgPerfil:{
        type:String,
        default:'/assets/profile.jpg'
    },
    fechaNacimiento:{
        type:Date,
        required:false
        //'La fecha de Nacimiento es obligatoria'
    },
    fechaRegistro:{
        type:Date,
        default:Date.now()
    },
    lugarOrigen:{
        type:String,
        required:false
    },
    genero:{
        type:String,
        required:[true,'El genero es obligatorio']
    },
    telefono:{
        type:String,
        required:[true,'El Telefono es obligatorio']
    },
    estado:{
        type:Boolean,
        default:true
    },
    favoritos:[]
});
 usuarioSchema.methods.toJSON = function() {

    let user = this;
    let userObject = user.toObject();
    delete userObject.contrasena;

    return userObject;
}
usuarioSchema.plugin(uniqueValidator,{
    message:'{PATH} debe ser unico'
});
module.exports = mongose.model('Usuario',usuarioSchema);